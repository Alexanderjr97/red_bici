"use strict";

var Bicicleta = function Bicicleta(id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function () {
  return 'id:' + this.id + "| color: " + this.color;
};

Bicicleta.allBicis = [];

Bicicleta.add = function (aBici) {
  var aBici = Bicicleta.allBicis.push(aBici);
};

Bicicleta.findById = function (idB) {
  var aBici = Bicicleta.allBicis.find(function (x) {
    return x.id == idB;
  });
  if (aBici) return aBici;else throw new Error("No se pudo encontrar ->>".concat(idB));
};

Bicicleta.removeById = function (aBiciId) {
  var aBici = Bicicleta.findById(aBiciId);

  for (var i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == aBiciId) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
};

exports.Bicicleta_delete_post = function (req, res) {
  Bicicleta.removeById(req.body.id);
  res.redirect('/bicicletas');
};

var a = new Bicicleta(1, 'rojo', 'extreme', [7.379240, -72.652442]);
var b = new Bicicleta(2, 'azul', 'clasica', [7.607858, -72.629723]);
Bicicleta.add(a);
Bicicleta.add(b);
module.exports = Bicicleta;