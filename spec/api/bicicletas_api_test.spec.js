var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
 

var base_url = "http://localhost:3000/api/bicicletas";

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongoDB://localhost/testdb';
        mongoose
    })
})

describe('Bicicleta API', () => {
    describe('Get Bicicleta /', () => {
        if ('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'Negra', 'Urbana', [11.12312, 12.121]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe('POST BICICLETA /create', () => {
       if ('Status 200', (done) => {
        var headers = {'content-type' : 'aplication/json'};    
        var aBici ='{ "id"  : 10, "color": "rojo", "modelo": "urbana", "lat": -34, "long":  -54}';
        
        request.post({
            headers: headers,
            url: 'https://localhost:3000/api/bicicletas/create',
            body: aBici
        }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });
});
