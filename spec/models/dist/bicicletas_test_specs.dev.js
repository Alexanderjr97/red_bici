"use strict";

var Bicicleta = require('/../Users/Alexander/red_bici/models/bicicleta');

describe('Bicileta.allBicis', function () {
  it ('comienza vacia', function () {
    expect(Bicicleta.allBicis.length).toBe(0);
  }) ;
});
describe('Bicicleta.add', function () {
  it ('agregamos una', function () {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(4, 'rojo', 'urbana'[(-34.13213, -58.1231)]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0].toBe(a));
  }) ;
});
describe('Bicicleta.findById', function () {
  it ('debe devolver la bici con id 1', function () {
    expect(Bicicleta.allBicis.length).toBe(0);
    Bicicleta.allBicis = [];
    var aBici = new Bicicleta(1, "verde", "urbana");
    var aBici2 = new Bicicleta(2, "Azul", "urbana");
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);
    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);
  }) ;
});